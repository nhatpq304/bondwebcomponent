import { Component } from '@angular/core';

@Component({
  selector: 'bond-first',
  template: 'first <router-outlet></router-outlet>',

})
export class FirstComponent {
}
