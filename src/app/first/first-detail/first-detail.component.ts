import { Component } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { map, Observable } from 'rxjs';

@Component({
  selector: 'bond-first-detail',
  template: 'detail id: {{id$ | async}}'
})
export class FirstDetailComponent {
  public readonly id$: Observable<string>;

  public constructor(private activatedRouter: ActivatedRoute) {
    this.id$ = this.activatedRouter.params.pipe(map((value:Params )=> value['id']));
  }
}
