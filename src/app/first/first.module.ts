import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FirstDetailComponent } from './first-detail/first-detail.component';
import { FirstComponent } from './first.component';

@NgModule({
  declarations: [
    FirstComponent,
    FirstDetailComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class FirstModule {
}
